## What’s changed

## ⬆️ Dependency updates

- ⬆️ Update zwave-js/zwave-js-ui to v9.7.1 @renovate ([#634](https://github.com/hassio-addons/addon-zwave-js-ui/pull/634))
- ⬆️ Update ghcr.io/hassio-addons/base-nodejs Docker tag to v0.1.2 @renovate ([#635](https://github.com/hassio-addons/addon-zwave-js-ui/pull/635))
